import { Component, OnInit } from '@angular/core';

import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { AppService } from './app.service';
import { OAuthProviderClientCredentials } from 'src/environments/environment';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html'
})
export class AppComponent implements OnInit {
  public appPages = [
    {
      title: 'Home',
      url: '/home',
      icon: 'home'
    },
    {
      title: 'List',
      url: '/list',
      icon: 'list'
    }
  ];

  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private readonly appService: AppService
  ) {
    this.initializeApp();
  }

  ngOnInit() {
    this.appService.initializeCodeGrant();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.appService.setupOauthConfig();
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }
}
