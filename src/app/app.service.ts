import { Injectable } from '@angular/core';
import { OAuthProviderClientCredentials, REDIRECT_PREFIX } from '../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AppService {
  state = '420';
  constructor() { }


  initializeCodeGrant() {

    if (!localStorage.getItem('code')) {
      const authRequestUrl =
      OAuthProviderClientCredentials.authorizationUrl +
      `?scope=${OAuthProviderClientCredentials.scope}&response_type=code&client_id=${
        OAuthProviderClientCredentials.clientId
      }&redirect_uri=${OAuthProviderClientCredentials.appUrl + REDIRECT_PREFIX
        }&state=${this.state}`;

      window.location.href = authRequestUrl;
    }

    return;
    }

    setupOauthConfig() {
      for (const key in OAuthProviderClientCredentials) {
        localStorage.setItem(key, OAuthProviderClientCredentials[key]);
      }
  }
  }

