import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { REDIRECT_PREFIX } from '../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { stringify } from 'querystring';

@Component({
  selector: 'app-callback',
  templateUrl: './callback.component.html',
  styleUrls: ['./callback.component.scss'],
})
export class CallbackComponent implements OnInit {

  constructor(
    private readonly activatedRout: ActivatedRoute,
    private readonly router: Router,
    private readonly http: HttpClient
  ) { }

  ngOnInit() {
    const queryParams = this.activatedRout.snapshot.queryParams;
    if (!queryParams.state || !queryParams.code) {
     this.failure();
    }
    localStorage.setItem('code', this.activatedRout.snapshot.queryParams.code);
    this.getRefreshToken(queryParams.code);
  }


  getRefreshToken(code) {
    const req: any = {
      grant_type: 'authorization_code',
      code,
      redirect_uri: localStorage.getItem('appUrl') + REDIRECT_PREFIX,
      client_id: localStorage.getItem('clientId'),
      scope: localStorage.getItem('scope'),
    };
    this.http.post<any>(localStorage.getItem('tokenUrl'), stringify(req), {
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
      }
    })
    .subscribe({
      next: response => {
        localStorage.setItem('accessToken' , response.access_token);
        localStorage.setItem('refreshToken' , response.refresh_token);
        localStorage.setItem('sub'  , response.sub);
        localStorage.setItem('roles' , response.roles);
        this.router.navigateByUrl('/home');
      },
      error: err => this.failure(),
    });
  }


  failure() {
    this.router.navigateByUrl('/home');
    return;
  }
}
